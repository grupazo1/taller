<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trabajo extends Model
{
    protected $fillable = [
        'nombre', 'idPrimerEstudiante', 'idSegundoEstudiante', 'idTercerEstudiante', 
        'idCuartoEstudiante', 'idPrimerProfesor', 'idSegundoProfesor', 'user_id', 'status', 'razonFinalizar'
    ];
}