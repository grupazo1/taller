<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;

use App\Http\Controllers\Controller;
use App\Avance; 
use App\Trabajo;
use App\User;
use App\Comentario;
use App\Notification;
use Illuminate\Support\Facades\Storage;
use Mail;


class AvanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trabajos = Trabajo::where('status','1')->get();
        $estudiantes = User::where('rol','Estudiante')->get();
        return view('avance.index', compact('trabajos','estudiantes'));
        //
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return;
    }

    public function crear($id){
        $trabajo = Trabajo::find($id);
        return view('avance.crear', compact('trabajo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('texto')){
            $avance = Avance::create($request->all());

            //IMAGE
            if($request->file('file')){
                if($request->input('nombre_file')){
                    $path = Storage::disk('public')->put('file', $request->file('file'));
                    $avance->fill(['file' => asset($path)])->save();
                } else {
                    return back()->with('error','Debe introducir un nombre del archivo.');
                }
            } else {
                if($request->input('nombre_file')){
                    return back()->with('error','Debe seleccionar un archivo si coloca un nombre.');
                }
            }

            //crear notificacion de registro de avance para profesor
            $notificacion = new Notification;
            $notificacion->trabajo_id = $avance->trabajo_id;
            $usuario = User::find($avance->user_id);
            $notificacion->autor_rol = $usuario->rol;
            $notificacion->texto = "Se ha realizado un registro de avance, id: ".$avance->id.", por: ".$usuario->name;
            $notificacion->save();

            $trabajo = Trabajo::find($notificacion->trabajo_id);
            $profesor1 = User::find($trabajo->idPrimerProfesor);
            $profesor2 = User::find($trabajo->idSegundoProfesor);

            if($profesor1){
                $emails[] = $profesor1->email;
            }
            if($profesor2){
                $emails[] = $profesor2->email;
            }
            Mail::send('mails.avance', [$emails], function ($m) use ($emails) {
                $m->from('bitacorawebicci@gmail.com', 'Bitacoras WEB');
        
                $m->to($emails)->subject('Se ha realizado un avance en un trabajo de titulacion!');
            }); 



            return redirect()->route('avance.index'); 
        } else {
            return back()->with('error','Debe introducir un mensaje en avance.');
        }
    }        

    public function storeComment(Request $request){
        $comentario = Comentario::create($request->all());
        return redirect()->route('avance.index'); 
    }

        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $avances = Avance::where('trabajo_id', $id)->get();
        $estudiantes = User::where('rol', 'Estudiante')->get();
        $profesores = User::where('rol','Profesor')->get();
        $trabajo = Trabajo::find($id);
        return view('avance.show', compact('avances','estudiantes','profesores','trabajo'));   
     }

     public function mostrar($id){

        $avance = Avance::find($id);
        $estudiantes = User::where('rol', 'Estudiante')->get();
        $profesores = User::where('rol','Profesor')->get();
        $trabajo = Trabajo::find($avance->trabajo_id);
        $comentarios = Comentario::where('avance_id',$id)->get();
 
        return view('avance.mostrar',compact('avance','estudiantes','profesores','trabajo','comentarios'));
     }

     public function agregarFile($id){
        
        $avance = Avance::find($id);
        $avances = Avance::where('trabajo_id',$avance->trabajo_id)->get();
        return view('avance.file',compact('avance','avances'));
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $avance = Avance::find($id);
        $avance->fill($request->all())->save();
        return redirect()->route('avance.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $avance = Avance::find($id);

        $nombre_file = $request->input('nombre_file');
        $file = $request->input('file');

        if($file and !$nombre_file) {
            return back()->with('error','Si selecciona un archivo debe colocar nombre.');
        } else if(!$file and $nombre_file){
            return back()->with('error','Si coloca un nombre debe seleccionar un archivo.');
        } else {
            $avance->fill($request->all())->save();
            return redirect()->route('avance.index');
        }      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
