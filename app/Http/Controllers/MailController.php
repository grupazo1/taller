<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller {

   public function html_email($nombre,$email) {
      $data = array('name'=>"Usuario");
      Mail::send('mail', $data, function($message) {
         $message->to($email, $nombre)->subject('Laravel HTML Testing Mail');
         $message->from('bitacorawebicci@gmail.com','Bitacora Web');
      });
   }

}