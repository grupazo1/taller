<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comentario;
use App\User;
use App\Notification;
use App\Avance;
use App\Trabajo;
use Mail;

class ComentarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comentario = Comentario::create($request->all());

        $notificacion = new Notification;

        $avance_id = $request->input('avance_id');
        $avance = Avance::find($avance_id);

        $notificacion->trabajo_id = $avance->trabajo_id;

        $user_id = $request->input('user_id');
        $usuario = User::find($user_id);
        $notificacion->texto = "Se ha registrado un comentario en el avance: ".$avance_id.", por: ".$usuario->name.".";
        $notificacion->autor_rol = $usuario->rol;
        $notificacion->save();

        $trabajo = Trabajo::find($avance->trabajo_id);
        $estudiante1 = User::find($trabajo->idPrimerEstudiante);
        $estudiante2 = User::find($trabajo->idSegundoEstudiante);
        $estudiante3 = User::find($trabajo->idTercerEstudiante);
        $estudiante4 = User::find($trabajo->idCuartoEstudiante);

        if($estudiante1){
            $emails[] = $estudiante1->email;
        }
        if($estudiante2){
            $emails[] = $estudiante2->email;
        }
        if($estudiante3){
            $emails[] = $estudiante3->email;
        }
        if($estudiante4){
            $emails[] = $estudiante4->email;
        }
        Mail::send('mails.comentario', [$emails], function ($m) use ($emails) {
            $m->from('bitacorawebicci@gmail.com', 'Bitacoras WEB');
    
            $m->to($emails)->subject('Se ha agregado un comentario al proyecto de titulación.');
            }); 
        

        return redirect()->route('avance.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
