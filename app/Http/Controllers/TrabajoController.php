<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trabajo; 
use App\User;
use App\Comentario;
use App\Avance;
use App\Notification;
use Mail;

class TrabajoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resoutce.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trabajos = trabajo::orderBy('id', 'DESC')->paginate();
        return view('trabajo.index', compact('trabajos'));
    }

    public function finalizar($id)
    {
        $trabajo = Trabajo::find($id);
        return view('trabajo.end', compact('trabajo'));          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estudiantes = User::where('rol','Estudiante')->get();
        $profesores = User::where('rol','Profesor')->get();
        //dd($profesores); sirve para ver los datos
        return view('trabajo.create', compact('estudiantes','profesores'));
    }


    public function alumnoDisponible($estudiante1){
        if($estudiante1->trabajando == '0'){
            return true;
        }
            return false; 
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estudiante1 = $request->input('idPrimerEstudiante');
        $estudiante2 = $request->input('idSegundoEstudiante');
        $estudiante3 = $request->input('idTercerEstudiante');
        $estudiante4 = $request->input('idCuartoEstudiante');
        
        $profesor1 = $request->input('idPrimerProfesor');
        $profesor2 = $request->input('idSegundoProfesor');

        if($estudiante1 != $estudiante2 and $estudiante1 != $estudiante3 and $estudiante1 != $estudiante4 and $estudiante2 != $estudiante3 and $estudiante2 != $estudiante4 and $estudiante3 != $estudiante4){
            
            if($estudiante1 != "nulo1" or $estudiante2 != "nulo2" or $estudiante3 != "nulo3" or $estudiante4 != "nulo4"){
                if($profesor1 != $profesor2){
                    if($profesor1 != "nulo1" or $profesor2 != "nulo2"){
                        $trabajo = Trabajo::create($request->all());
                        //los estudiantes comienzan a trabajar
                        if($request->input('idPrimerEstudiante') != "nulo1"){
                            $usuario = User::find($trabajo->idPrimerEstudiante);
                            $usuario->trabajando = "1";
                            $usuario->save();
                        }
                        if($request->input('idSegundoEstudiante') != "nulo2"){
                            $usuario2 = User::find($trabajo->idSegundoEstudiante);
                            $usuario2->trabajando = "1";
                            $usuario2->save();
                        }
                        if($request->input('idTercerEstudiante') != "nulo3"){
                            $usuario3 = User::find($trabajo->idTercerEstudiante);
                            $usuario3->trabajando = "1";
                            $usuario3->save();
                        }
                        if($request->input('idCuartoEstudiante') != "nulo4"){
                            $usuario4 = User::find($trabajo->idCuartoEstudiante);
                            $usuario4->trabajando = "1";
                            $usuario4->save();
                        }
                        return redirect()->route('trabajo.index');  
                    } else {
                        return back()->with('error','Debe seleccionar al menos un profesor.');
                    }
                    
                } else {
                    return back()->with('error','Los profesores no deben repetirse en las casillas.');
                }
                
            } else {
                return back()->with('error','Debe seleccionar al menos un estudiante.');
            }
            
        } else {
            return back()->with('error','Los estudiantes no deben repetirse en las casillas.');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trabajo = Trabajo::find($id);
        $estudiantes = User::where('rol','Estudiante')->get();
        $profesores = User::where('rol','Profesor')->get();

        return view('trabajo.show', compact('trabajo','estudiantes','profesores')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trabajo = Trabajo::find($id);
        $estudiantes = User::where('rol','Estudiante')->get();
        $profesores = User::where('rol','Profesor')->get();

        return view('trabajo.edit', compact('trabajo','estudiantes','profesores'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesor1 = $request->input('idPrimerProfesor');
        $profesor2 = $request->input('idSegundoProfesor');

        if($profesor1 != $profesor2){
            if($profesor1 != "nulo1" or $profesor2 != "nulo2"){
                $trabajo = Trabajo::find($id);
                $trabajo->fill($request->all())->save();
                return redirect()->route('trabajo.index');
            } else {
                return back()->with('error','Debe seleccionar al menos un profesor.');
            }
        } else {
            return back()->with('error','Los profesores no deben repetirse en las casillas.');
        }
    }


    public function terminar(Request $request,$id){
        $trabajo = Trabajo::find($id);
        

        $booleanFinalizar = $request->input('status');
        $razonFinalizar = $request->input('razonFinalizar');

        if($booleanFinalizar == "0" and $razonFinalizar == "nulo1"){
            return back()->with('error','No se puede eliminar sin razón.');
        } elseif(($booleanFinalizar == "1" and $razonFinalizar == "Trabajo no continuado") or ($booleanFinalizar == "1" and $razonFinalizar == "Trabajo aprobado")){
            return back()->with('error','No debe colocar una razón si no desea eliminar.');
        }

        if($request->input('status') == '0'){
            if($trabajo->idPrimerEstudiante != "nulo1"){
                $usuario = User::find($trabajo->idPrimerEstudiante);
                $usuario->trabajando = "0";
                $usuario->save();
            }
            if($trabajo->idSegundoEstudiante != "nulo2"){
                $usuario2 = User::find($trabajo->idSegundoEstudiante);
                $usuario2->trabajando = "0";
                $usuario2->save();
            }
            if($trabajo->idTercerEstudiante != "nulo3"){
                $usuario3 = User::find($trabajo->idTercerEstudiante);
                $usuario3->trabajando = "0";
                $usuario3->save();
            }
            if($trabajo->idCuartoEstudiante != "nulo4"){
                $usuario4 = User::find($trabajo->idCuartoEstudiante);
                $usuario4->trabajando = "0";
                $usuario4->save();
            }
        }

        $notificacion = new Notification;
        $notificacion->trabajo_id = $trabajo->id;
        $notificacion->autor_rol = "Secretaria";

        $notificacion->texto = "Se ha finalizado el trabajo de titulación: ".$razonFinalizar.".";
        $notificacion->save();

    
        $estudiante1 = User::find($trabajo->idPrimerEstudiante);
        $estudiante2 = User::find($trabajo->idSegundoEstudiante);
        $estudiante3 = User::find($trabajo->idTercerEstudiante);
        $estudiante4 = User::find($trabajo->idCuartoEstudiante);
        $profesor1 = User::find($trabajo->idPrimerProfesor);
        $profesor2 = User::find($trabajo->idSegundoProfesor);

        if($estudiante1){
            $emails[] = $estudiante1->email;
        }
        if($estudiante2){
            $emails[] = $estudiante2->email;
        }
        if($estudiante3){
            $emails[] = $estudiante3->email;
        }
        if($estudiante4){
            $emails[] = $estudiante4->email;
        }
        if($profesor1){
            $emails[] = $profesor1->email;
        }
        if($profesor2){
            $emails[] = $profesor2->email;
        }
        
        
        if($razonFinalizar == "Trabajo aprobado"){
            Mail::send('mails.aprobado', [$emails], function ($m) use ($emails) {
                $m->from('bitacorawebicci@gmail.com', 'Bitacoras WEB');
        
                $m->to($emails)->subject('Se ha finalizado el trabajo trabajo de titulación!');
                }); 
            
    
            $trabajo->fill($request->all())->save();
            return redirect()->route('trabajo.index');
    
        }
        else if($razonFinalizar == "Trabajo no continuado") {
            Mail::send('mails.no_continuado', [$emails], function ($m) use ($emails) {
                $m->from('bitacorawebicci@gmail.com', 'Bitacoras WEB');
        
                $m->to($emails)->subject('Se ha finalizado el trabajo trabajo de titulación!');
                }); 
            
    
            $trabajo->fill($request->all())->save();
            return redirect()->route('trabajo.index');
        }
        
    }

    


    public function ConsultarBitacoras($id){
        $usuario = User::find($id);
        $trabajos = Trabajo::all();
        $estudiantes = User::where('rol','Estudiante')->get();
        $profesores = User::where('rol','Profesor')->get();
        $avances = Avance::all();

        return view('consultar.index', compact('usuario','trabajos','estudiantes','profesores','avances')); 

    }


    public function VerEvidencia($id){
        $trabajo = Trabajo::find($id);
        $usuario = User::find($trabajo->user_id);
        $estudiantes = User::where('rol','Estudiante')->get();
        $profesores = User::where('rol','Profesor')->get();
        $avances = Avance::orderBy('id', 'DESC')->paginate();

        return view('consultar.show', compact('usuario','trabajo','estudiantes','profesores','avances')); 

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Trabajo::find($id)->delete();
        return back();
    }

}
