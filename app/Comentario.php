<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $fillable = [
        'id', 'texto', 'avance_id', 'user_id',
    ];
}
