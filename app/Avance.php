<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avance extends Model
{
    protected $fillable = [
        'id', 'texto', 'file','nombre_file', 'trabajo_id', 'user_id',
    ];
}
