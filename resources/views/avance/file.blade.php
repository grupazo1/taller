@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    ASOCIAR ARCHIVO
                    <a href="{{route('avance.index')}}" style="position: absolute; right: 150px;" class="btn btn-info">Volver</a>
                </div>
                <div class="card-body">
                    {!! Form::model($avance, ['route' => ['avance.update', $avance->id], 'method' => 'PUT']) !!}
                    

                        <!-- Agregar un archivo antiguo subido o nuevo -->
                            <div class="form-group">
                                    @foreach($avances as $avanceIndividual)
                                        @if($avanceIndividual->file)
                                        <input class="w3-radio" type="radio" name="file" value="{{$avanceIndividual->file}}" checked>
                                        <label><iframe src="{{$avanceIndividual->file}}" width="100%" height="300" style="border:1px solid black;"></iframe></label>
                                        <br>
                                        @endif
                                    @endforeach

                                    <input class="w3-radio" type="radio" name="file" value="" checked>
                                    <label>Ningún archivo.</label>
                            </div> 

                            <div class="form-group">
                                {{       Form::label('nombre_file', 'Ingrese nombre de evidencia:')     }}
                                {{       Form::text('nombre_file',null,['class' => 'form-control'])     }}
                            </div>
        

                    <div class="form-group">
                        {{       Form::submit('Guardar', ['class' => 'btn btn-primary'])     }}
                    </div>
                   
                    {!! Form::close() !!}
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection