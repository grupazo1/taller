
@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row">
            <div class='col-md-8 col-md-offset-2'>
                <div class="card">
                    <div class="card-header">
                          Revisar avance 
                            @if(Auth::user()->rol == 'Estudiante')
                                <a href="{{route('creando', $trabajo->id)}}" style="float:right"class="btn bn-sm btn-primary">Crear</a>
                            @endif
                    </div>
                </div>

                <div class="card-body">           
                    <table class='table table-striped table-hover'>
                        <thread>
                            <tr>
                                <th>ID avance</th>
                                <th>Nombre alumno</th>
                                <th>Fecha creación</th>
                                <th>Acciones</th>
                            </tr>
                        </thread>

                        <Tbody>
                            @foreach($avances as $avance)
                                <tr>
                                    <td>{{$avance->id}}</td> 

                                    @foreach($estudiantes as $estudiante)
                                        @if($avance->user_id == $estudiante->id)
                                            <td>{{$estudiante->name}}</td> 
                                        @endif   
                                    @endforeach
                                    <td>{{$avance->created_at}}</td>
                                    <td>
                                    <a  href="{{route('avance.mostrar', $avance->id)}}" class="btn btn-sm btn-primary">Ver avance</a>
                                    @if(!$avance->file)
                                        @if(Auth::user()->rol == 'Estudiante')
                                            <a  href="{{route('agregarFile', $avance->id)}}" class="btn btn-sm btn-primary">Asociar archivo</a>
                                        @endif
                                    @endif    
                                    </td>      
                                    
                                </tr>     
                            @endforeach
                                                                        
                        </Tbody>

                    </table>
                </div>
                <a href="{{route('avance.index')}}" class="btn btn-info">Volver</a>
            </div>
        </div>


    </div>



@endsection