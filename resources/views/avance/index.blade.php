@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row">
            <div class='col-md-8 col-md-offset-2'>
                <div class="card">
                    <div class="card-header">
                        Trabajo de alumno
 
                    </div>
                </div>

                <div class="card-body">           
                    <table class='table table-striped table-hover'>
                        <thread>
                            <tr>
                                <th>ID</th>
                                <th>Nombre trabajo</th>
                                <th>Acciones</th>
                            </tr>
                        </thread>

                        <Tbody>
                            @foreach($trabajos as $trabajo)
                                @if($trabajo->status == '1')
                                    @if($trabajo->idPrimerEstudiante == Auth::user()->id or $trabajo->idSegundoEstudiante == Auth::user()->id or $trabajo->idTercerEstudiante == Auth::user()->id or $trabajo->idCuartoEstudiante == Auth::user()->id or $trabajo->idPrimerProfesor == Auth::user()->id or $trabajo->idSegundoProfesor == Auth::user()->id or Auth::user()->id == 'Encargado' or Auth::user()->id == 'Encargado/profesor')
                                    <tr>
                                        <td>{{$trabajo->id}}</td>   
                                        <td>{{$trabajo->nombre}}</td>  
                                        <td> 
                                                                                        
                                            <a  href="{{route('avance.show', $trabajo->id)}}" class="btn btn-sm btn-primary">Ver avances</a>
                                            @if(Auth::user()->id == $trabajo->idPrimerEstudiante or Auth::user()->id == $trabajo->idSegundoEstudiante or Auth::user()->id == $trabajo->idTercerEstudiante or Auth::user()->id == $trabajo->idCuartoEstudiante)
                                                <a  href="{{route('creando', $trabajo->id)}}" class="btn btn-sm btn-primary">Registrar avance</a> 
                                            @endif
                                        
                                        </td>   
                                    </tr> 
                                    @endif    
                                @endif         
                            @endforeach
                                                                        
                        </Tbody>

                    </table>
                </div>

            </div>
        </div>


    </div>

<script>

function confirmar_accion(){

    return confirm('¿Estás seguro que deseas eliminar este registro?');
    
}

</script>

@endsection