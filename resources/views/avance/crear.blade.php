@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                        Crear nuevo avance
                </div>
                <div class="card-body">

                {!! Form::open(['route' => 'avance.store', 'files'=> true])!!}

                    {{ Form::hidden('user_id', auth()->user()->id)}}
                    {{ Form::hidden('trabajo_id', $trabajo->id)}}

                    <div class="form-group">
                        {{       Form::label('parametro1', 'Ingrese avance:')     }}
                        {{       Form::text('texto',null,['class' => 'form-control'])     }}
                    </div>

                    <div class="form-group">
                        {{       Form::label('file', 'Evidencia')     }}
                        {{       Form::file('file')     }}
                    </div>

                    <div class="form-group">
                        {{       Form::label('nombre_file', 'Ingrese nombre de evidencia:')     }}
                        {{       Form::text('nombre_file',null,['class' => 'form-control'])     }}
                    </div>

                    <div class="form-group">
                        {{       Form::submit('Guardar', ['class' => 'btn btn-primary'])     }}
                    </div>

                {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection