@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    VER AVANCE
                    <a href="{{route('trabajo.index')}}" style="position: absolute; right: 150px;" class="btn btn-info">Volver</a>
                </div>
                <div class="card-body">
                <br>
                <p><strong>Avance texto: </strong>{{$avance->texto}}</p>
                <br>
                <p><strong>Evidencia: {{$avance->nombre_file}}</strong></p>
                @if($avance->file)
                    <iframe src="{{$avance->file}}" width="100%" height="300" style="border:1px solid black;"></iframe>
                    @if(Auth::user()->id == 'Encargado' or Auth::user()->id == 'Encargado/profesor' or Auth::user()->id == $trabajo->idPrimerProfesor or Auth::user()->id == $trabajo->idSegundoProfesor)
                        <a href="{{$avance->file}}" download>
                            Descargar archivo.
                        </a>
                    @endif
                    <br><br>
                @endif

                
                <table class='table table-striped table-hover'> 
                    <thread>
                        <tr>
                            <th>Comentario</th>
                            <th>Publicado por</th>
                        </tr>
                    </thread>



                    <Tbody>

                    @foreach ($comentarios as $comentario)
                        <tr>
                            <td>{{$comentario->texto}}</td>
                            @foreach($profesores as $profesor)
                                @if($profesor->id == $comentario->user_id)
                                    <td>{{$profesor->name}}</td>
                                @endif
                            @endforeach
                        </tr>
                        @endforeach
                    </Tbody>
                </table>
              

                @if(Auth::user()->id == 'Encargado' or Auth::user()->id == 'Encargado/profesor' or Auth::user()->id == $trabajo->idPrimerProfesor or Auth::user()->id == $trabajo->idSegundoProfesor)
                    {!! Form::open(['route' => 'comentario.store'])!!}
                    {{ Form::hidden('user_id', auth()->user()->id)}}
                    {{ Form::hidden('avance_id',$avance->id)}}

                        <div class="form-group">
                            {{       Form::label('parametro1', 'Ingrese comentario:')     }}
                            {{       Form::text('texto',null,['class' => 'form-control'])     }}
                        </div>

                        <div class="form-group">
                            {{       Form::submit('Guardar', ['class' => 'btn btn-primary'])     }}
                        </div>
                                              
                    {!! Form::close() !!}
                @endif

            </div>
        </div>
    </div>
</div>

@endsection