<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>  


        <!-- Styles -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    </head>
    <body>
    <div id="app">
        <nav style="margin-bottom:0px" class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}"> BITACORAS</a>
                <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                    @guest
                            <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ route('login') }}">Ingresar</a></li>
                    @else
                            <li class="nav-item mx-0 mx-lg-1 dropdown">
                                <a href="#" class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li class="nav-item mx-0 mx-lg-1">
                                        @if(Auth::user()->rol=='Administrador')
                                            <a href="{{ route('usuario.index') }}">
                                                Crear Usuario
                                            </a>
                                        @endif

                                        @if (Auth::user()->rol=="Secretaria" || Auth::user()->rol=="Encargado" || Auth::user()->rol=="Encargado/profesor" )
                                            <a href="{{ route('trabajo.index') }}" style="padding-right: 20px;">Trabajos</a>
                                        @endif

                                        @if (Auth::user()->rol=="Estudiante")
                                            <li><a href="{{ route('avance.index') }}" style="padding-right: 20px;">Trabajo</a></li>
                                            <li><a href="{{ route('notification.index') }}" style="padding-right: 20px;">Notificaciones</a></li>
                                            <li><a href="{{ route('consultar', Auth::user()->id) }}" style="padding-right: 20px;">Consultar bitácora</a></li>
                                        @endif

                                        @if (Auth::user()->rol=="Encargado" || Auth::user()->rol=="Encargado/profesor" || Auth::user()->rol=="Profesor" )
                                    
                                            <li><a href="{{ route('avance.index') }}" style="padding-right: 20px;">Avances</a></li>
                                            <li><a href="{{ route('notification.index') }}" style="padding-right: 20px;">Notificaciones</a></li>
                                            <li><a href="{{ route('consultar', Auth::user()->id) }}" style="padding-right: 20px;">Consultar bitácora</a></li>
                                        @endif

                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>

                        @endguest
                        
                    </ul>
                </div>
            </div>
        </nav>
        <header class="masthead bg-primary text-white text-center">
            <div class="container d-flex align-items-center flex-column">
                <!-- Masthead Avatar Image-->
                <img class="masthead-avatar mb-5" src="{{ asset('assets/img/avataaars.svg') }}" alt="" />
                <!-- Masthead Heading-->
                <h1 class="masthead-heading text-uppercase mb-0">BIENVENIDO A BITACORAS DISC</h1>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Masthead Subheading-->
                <p class="masthead-subheading font-weight-light mb-0"></p>
            </div>
        </header>

    <!-- About Section-->
    <section class="page-section bg-info text-white mb-0" id="about">
        <div class="container">
            <!-- About Section Heading-->
            <h2 class="page-section-heading text-center text-uppercase text-white">BITACORAS DISC</h2>
            <!-- Icon Divider-->
            <div class="divider-custom divider-light">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                <div class="divider-custom-line"></div>
            </div>
            <!-- About Section Content-->
            <div class="row">
                <div class="col-lg-8 ml-auto"><p class="lead">Aqui se haran las revisiones de bitacoras</p></div>
            </div>
        </div>
    </section>

    <footer class="footer text-center">
        <div class="container">
            <div class="row">
                <!-- Footer Location-->
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <h4 class="text-uppercase mb-4">UBICACIÓN</h4>
                    <p class="lead mb-0">
                        UCN - Av.Angamos 0610
                        <br />
                        Departamento de Ingeniería de Sistemas y Computación Y1
                    </p>
                </div>
                <!-- Footer Social Icons-->
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <h4 class="text-uppercase mb-4">NUESTRAS REDES</h4>
                    <a class="btn btn-outline-light btn-social mx-1" href="https://www.facebook.com/pages/category/College---University/Carrera-Ingenier%C3%ADa-Civil-en-Computaci%C3%B3n-e-Inform%C3%A1tica-UCN-432851210086760/"><i class="fab fa-fw fa-facebook-f"></i></a>
                    <a class="btn btn-outline-light btn-social mx-1" href="https://twitter.com/carreraicciucn?lang=es"><i class="fab fa-fw fa-twitter"></i></a>
                    <a class="btn btn-outline-light btn-social mx-1" href="http://www.disc.ucn.cl/"><i class="fab fa-fw fa-dribbble"></i></a>
                </div>
                <!-- Footer About Text-->
                <div class="col-lg-3">
                    <h4 class="text-uppercase mb-4">Acerca de</h4>
                    <p class="lead mb-0">
                        Bitacoras UCN fue creado por un grupo de estudiantes en un proyecto para Ing. de Software
                    </p>
                </div>
            </div>
        </div>
    </footer>


    </div>

<!-- Scripts -->
    
<script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Contact form JS-->
    <script src="{{ asset('assets/mail/jqBootstrapValidation.jss') }}"></script>
    <script src="{{ asset('assets/mail/contact_me.js') }}"></script>
    <!-- Core theme JS-->


    </body>
</html>
