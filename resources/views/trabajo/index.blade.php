@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class='col-md-8 col-md-offset-2'>
                <div class="card">
                    <div class="card-header">
                        Lista de trabajos
                            <a href="{{route('trabajo.create')}}" style="float:right"class="btn bn-sm btn-primary">Crear</a>
                    </div>
                </div>

                <div class="card-body">           
                    <table class='table table-striped table-hover'>
                        <thread>
                            <tr>
                                <th>ID</th>
                                <th>Nombre trabajo</th>
                                <th>Usuario</th>
                                <th>Acciones</th>
                                <th>Estado</th>
                                <th style="text-align:center">Razón Finalización</th>
                            </tr>
                        </thread>

                        <Tbody>
                            @foreach($trabajos as $trabajo)
                                <tr>
                                    <td>{{$trabajo->id}}</td>   
                                    <td>{{$trabajo->nombre}}</td>     
                                    <td>{{App\User::find($trabajo->user_id)->name}}</td> 
                                    <td> 
                                        <a  href="{{route('trabajo.show', $trabajo->id)}}" class="btn btn-sm btn-primary">Ver</a>
                                        @if($trabajo->status == "1")
                                            <a  href="{{route('trabajo.edit', $trabajo->id)}}" class="btn btn-sm btn-primary">Editar</a >                                       
                                            <a  href="{{route('finalizar', $trabajo->id)}}" class="btn btn-sm btn-danger">Finalizar</a>
                                        @endif
                                    </td> 
                                    <td>
                                        @if($trabajo->status == "1")
                                            En proceso
                                        @endif
                                        @if($trabajo->status == "0")
                                            Finalizado
                                        @endif
                                    </td>
                                    <td style="text-align:center">
                                        @if($trabajo->razonFinalizar == "nulo1" or $trabajo->razonFinalizar == null)
                                            -
                                        @else
                                            {{$trabajo->razonFinalizar}}
                                        @endif
                                    </td>
                                </tr>              
                            @endforeach
                                                                        
                        </Tbody>

                    </table>
                </div>

            </div>
        </div>


    </div>

<script>

function confirmar_accion(){

    return confirm('¿Estás seguro que deseas eliminar este registro?');

}

</script>

@endsection