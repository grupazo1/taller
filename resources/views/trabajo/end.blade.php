@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    FINALIZAR TRABAJO
                    <a href="{{route('trabajo.index')}}" style="position: absolute; right: 150px;" class="btn btn-info">Volver</a>
                </div>
                <div class="card-body">
                    {!! Form::model($trabajo, ['route' => ['terminar', $trabajo->id], 'method' => 'PUT']) !!}
                    
                     <!-- Opción de modificar a primer profesor-->
                    <div class="form-group">
                            <label for="status">¿Desea finalizar este trabajo?</label>
                                <select class= "form-control" name="status" id="status" required>
                                        <option value="1">No</option>
                                        <option value="0">Sí</option>
                                </select>
                            <br>
                            <label for="razonFinalizar">¿Por qué desea terminar el trabajo?</label>
                                <select class= "form-control" name="razonFinalizar" id="razonFinalizar" required>
                                    <option value="nulo1">No deseo eliminar</option>
                                    <option value="Trabajo no continuado">No continuidad del trabajo</option>
                                    <option value="Trabajo aprobado">Aprobación del trabajo</option>
                                </select>
                    </div>

                    <div class="form-group">
                        {{       Form::submit('Guardar', ['class' => 'btn btn-primary'])     }}
                    </div>
                   
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection