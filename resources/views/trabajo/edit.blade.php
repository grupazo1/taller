@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    EDITAR TRABAJO
                    <a href="{{route('trabajo.index')}}" style="position: absolute; right: 150px;" class="btn btn-info">Volver</a>
                </div>
                <div class="card-body">
                    {!! Form::model($trabajo, ['route' => ['trabajo.update', $trabajo->id], 'method' => 'PUT']) !!}
                    
                    <!-- Ingresar nombre de trabajo -->
                    <div class="form-group">
                        {{       Form::label('parametro1', 'Ingrese nombre del trabajo')     }}
                        {{       Form::text('nombre',null,['class' => 'form-control', 'required' => 'required'])     }}
                    </div>
                    
                    @foreach($estudiantes as $estudiante)
                        <!-- Opción de eliminar a primer estudiante -->
                        @if($estudiante->id == $trabajo->idPrimerEstudiante)
                            <div class="form-group">
                                <label for="primerEstudiante">¿Eliminar a {{$estudiante->name}}?</label>
                                    <select class= "form-control" name="idPrimerEstudiante" id="idPrimerEstudiante" required>
                                                    <option value="{{$trabajo->idPrimerEstudiante}}"> No </option>
                                                    <option value="nulo1"> Sí </option>
                                    </select>
                            </div>
                        @endif
                        <!-- Opción de eliminar a segundo estudiante -->
                        @if($estudiante->id == $trabajo->idSegundoEstudiante)
                            <div class="form-group">
                                <label for="segundoEstudiante">¿Eliminar a {{$estudiante->name}}?</label>
                                    <select class= "form-control" name="idSegundoEstudiante" id="idSegundoEstudiante" required>
                                                    <option value="{{$trabajo->idSegundoEstudiante}}"> No </option>
                                                    <option value="nulo2"> Sí </option>
                                    </select>
                            </div>
                        @endif

                        <!-- Opción de eliminar a tercer estudiante -->
                        @if($estudiante->id == $trabajo->idTercerEstudiante)
                            <div class="form-group">
                                <label for="tercerEstudiante">¿Eliminar a {{$estudiante->name}}?</label>
                                    <select class= "form-control" name="idTercerEstudiante" id="idTercerEstudiante" required>
                                                    <option value="{{$trabajo->idTercerEstudiante}}"> No </option>
                                                    <option value="nulo3"> Sí </option>
                                    </select>
                            </div>
                        @endif

                        <!-- Opción de eliminar a cuarto estudiante -->
                        @if($estudiante->id == $trabajo->idCuartoEstudiante)
                            <div class="form-group">
                                <label for="cuartoEstudiante">¿Eliminar a {{$estudiante->name}}?</label>
                                    <select class= "form-control" name="idCuartoEstudiante" id="idCuartoEstudiante" required>
                                                    <option value="{{$trabajo->idCuartoEstudiante}}"> No </option>
                                                    <option value="nulo4"> Sí </option>
                                    </select>
                            </div>
                        @endif

                    @endforeach

                    <!-- Opción de modificar a primer profesor -->
                    @if($trabajo->idPrimerProfesor != "nulo1")
                        <div class="form-group">
                                <label for="primerProfesor">Modificar profesor guía</label>
                                    <select class= "form-control" name="idPrimerProfesor" id="idPrimerProfesor">
                                        @foreach ($profesores as $profesor)
                                            @if($trabajo->idPrimerProfesor == $profesor->id)
                                                <option value="{{$trabajo->idPrimerProfesor}}">{{$profesor->name}}</option>
                                            @endif
                                        @endforeach
                                        @foreach ($profesores as $profesor)
                                            @if($profesor->disponible == '1' and $profesor->id != $trabajo->idPrimerProfesor)
                                                        <option value="{{$profesor->id}}">{{$profesor->name}}</option>
                                            @endif
                                        @endforeach
                                            <option value="nulo1">Eliminar profesor</option>
                                    </select>
                        </div>
                    @endif
                    
                    <!-- En caso de tener solo 1 profesor y querer agregar otro -->
                    @if($trabajo->idPrimerProfesor == "nulo1")
                        <div class="form-group">
                            <label for="primerProfesor">Agregar profesor guía</label>
                                <select class= "form-control" name="idPrimerProfesor" id="idPrimerProfesor" required>
                                    <option value="nulo1">Seleccione profesor</option>
                                    @foreach ($profesores as $profesor)
                                        @if($profesor->disponible == '1')
                                            <option value="{{$profesor->id}}">{{$profesor->name}} - Rut: {{$profesor->rut}}</option>
                                        @endif
                                    @endforeach
                                </select>
                        </div>
                    @endif
                    
                    <!-- Opción de modificar a segundo profesor -->
                    @if($trabajo->idSegundoProfesor != "nulo2")
                        <div class="form-group">
                                <label for="primerProfesor">Modificar segundo profesor guía</label>
                                <select class= "form-control" name="idSegundoProfesor" id="idSegundoProfesor">
                                    @foreach ($profesores as $profesor)
                                        @if($trabajo->idSegundoProfesor == $profesor->id)
                                            <option value="{{$trabajo->idSegundoProfesor}}">{{$profesor->name}}</option>
                                        @endif
                                    @endforeach
                                    @foreach ($profesores as $profesor)
                                        @if($profesor->disponible == '1' and $profesor->id != $trabajo->idSegundoProfesor)
                                                    <option value="{{$profesor->id}}">{{$profesor->name}}</option>
                                        @endif
                                    @endforeach
                                    <option value="nulo2">Eliminar profesor</option>
                                </select>
                        </div>
                    @endif

                    <!-- En caso de tener solo 1 profesor y querer agregar otro -->
                    @if($trabajo->idSegundoProfesor == "nulo2")
                        <div class="form-group">
                            <label for="primerProfesor">Agregar segundo profesor guía</label>
                                <select class= "form-control" name="idSegundoProfesor" id="idSegundoProfesor" required>
                                <option value="nulo2">Seleccione profesor</option>
                                    @foreach ($profesores as $profesor)
                                            @if($profesor->disponible == '1')
                                                <option value="{{$profesor->id}}">{{$profesor->name}} - Rut: {{$profesor->rut}}</option>
                                            @endif
                                    @endforeach
                                </select>
                        </div>
                    @endif

                    <div class="form-group">
                        {{       Form::submit('Guardar', ['class' => 'btn btn-primary'])     }}
                    </div>
                   
                    {!! Form::close() !!}
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection