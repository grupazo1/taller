@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                        Crear nuevo trabajo
                </div>
                <div class="card-body">
                        {!! Form::open(['route' => 'trabajo.store'])!!}
                        {{ Form::hidden('user_id', auth()->user()->id)}}
                        {{ Form::hidden('status', 1)}}

                        <div class="form-group">
                            {{       Form::label('parametro1', 'Ingrese nombre del trabajo')     }}
                            {{       Form::text('nombre',null,['class' => 'form-control', 'required' => 'required'])     }}
                         </div>
                        
                        <div class="form-group">
                            <label for="primerEstudiante">Primer estudiante</label>
                                <select class= "form-control" name="idPrimerEstudiante" id="idPrimerEstudiante" required>
                                    <option value="nulo1">Seleccione estudiante</option>
                                    @foreach ($estudiantes as $estudiante)
                                            @if($estudiante->disponible == '1' and $estudiante->trabajando == '0')
                                                <option value="{{$estudiante->id}}">{{$estudiante->name}} - Rut: {{$estudiante->rut}} - Carrera: {{$estudiante->carrera}}</option>
                                            @endif
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                            <label for="segundoEstudiante">Segundo estudiante</label>
                                <select class= "form-control" name="idSegundoEstudiante" id="idSegundoEstudiante">
                                <option value="nulo2">Seleccione estudiante</option>
                                    @foreach ($estudiantes as $estudiante)
                                            @if($estudiante->disponible == '1' and $estudiante->trabajando == '0')
                                                <option value="{{$estudiante->id}}">{{$estudiante->name}} - Rut: {{$estudiante->rut}} - Carrera: {{$estudiante->carrera}}</option>
                                            @endif
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                            <label for="tercerEstudiante">Nombre tercer estudiante</label>
                                <select class= "form-control" name="idTercerEstudiante" id="idTercerEstudiante">
                                <option value="nulo3">Seleccione estudiante</option>
                                    @foreach ($estudiantes as $estudiante)
                                            @if($estudiante->disponible == '1' and $estudiante->trabajando == '0')
                                                <option value="{{$estudiante->id}}">{{$estudiante->name}} - Rut: {{$estudiante->rut}} - Carrera: {{$estudiante->carrera}}</option>
                                            @endif
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                            <label for="cuartoEstudiante">Nombre cuarto estudiante</label>
                                <select class= "form-control" name="idCuartoEstudiante" id="idCuartoEstudiante">
                                <option value="nulo4">Seleccione estudiante</option>
                                    @foreach ($estudiantes as $estudiante)
                                            @if($estudiante->disponible == '1' and $estudiante->trabajando == '0')
                                                <option value="{{$estudiante->id}}">{{$estudiante->name}} - Rut: {{$estudiante->rut}} - Carrera: {{$estudiante->carrera}}</option>
                                            @endif
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                            <label for="primerProfesor">Nombre profesor guía</label>
                                <select class= "form-control" name="idPrimerProfesor" id="idPrimerProfesor" required>
                                <option value="nulo1">Seleccione profesor</option>
                                    @foreach ($profesores as $profesor)
                                            @if($profesor->disponible == '1' and $profesor->trabajando == '0')
                                                <option value="{{$profesor->id}}">{{$profesor->name}} - Rut: {{$profesor->rut}}</option>
                                            @endif
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                            <label for="segundoProfesor">Nombre segundo profesor guía</label>
                                <select class= "form-control" name="idSegundoProfesor" id="idSegundoProfesor">
                                <option value="nulo2">Seleccione profesor</option>
                                    @foreach ($profesores as $profesor)
                                            @if($profesor->disponible == '1' and $profesor->trabajando == '0')
                                                <option value="{{$profesor->id}}">{{$profesor->name}} - Rut: {{$profesor->rut}}</option>
                                            @endif
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                            {{       Form::submit('Guardar', ['class' => 'btn btn-primary'])     }}
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection