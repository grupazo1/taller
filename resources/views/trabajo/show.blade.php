@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="style.css">


<a href="{{route('trabajo.edit', $trabajo->id)}}" style="float: right; padding: 20px; margin: 0 9%" class="btn btn-primary" >Editar</a>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                
                <div class="card-header">
                    <p><strong>Trabajo de titulación: </strong>{{$trabajo->nombre}}</p>
                    <strong>Datos estudiantes</strong>
                    
                </div>
            </div>
                <div class="card-body">
                    <table class='table table-striped table-hover'> 

                        <thread>
                            <tr>
                                <th>Nombre</th>
                                <th>Rut</th>
                                <th>Carrera</th>
                            </tr>
                        </thread>

                    

                        <Tbody>

                            @foreach ($estudiantes as $estudiante)
                                <tr>
                                        @if($estudiante->id == $trabajo->idPrimerEstudiante)
                                            <td>{{$estudiante->name}}</td>
                                            <td>{{$estudiante->rut}}</td>
                                            <td>{{$estudiante->carrera}}</td>
                                        @endif
                                        @if($estudiante->id == $trabajo->idSegundoEstudiante)
                                            <td>{{$estudiante->name}}</td>
                                            <td>{{$estudiante->rut}}</td>
                                            <td>{{$estudiante->carrera}}</td>
                                        @endif
                                        @if($estudiante->id == $trabajo->idTercerEstudiante)
                                            <td>{{$estudiante->name}}</td>
                                            <td>{{$estudiante->rut}}</td>
                                            <td>{{$estudiante->carrera}}</td>
                                        @endif
                                        @if($estudiante->id == $trabajo->idCuartoEstudiante)
                                            <td>{{$estudiante->name}}</td>
                                            <td>{{$estudiante->rut}}</td>
                                            <td>{{$estudiante->carrera}}</td>
                                        @endif
                                </tr>
                            @endforeach

                        </Tbody>
                    </table>
                
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    <strong>Datos profesores</strong>
                </div>
            </div>
                <div class="card-body">
                    <table class='table table-striped table-hover'> 

                        <thread>
                            <tr>
                                <th>Nombre</th>
                                <th>Rut</th>
                            </tr>
                        </thread>

                    

                        <Tbody>

                            @foreach ($profesores as $profesor)
                                <tr>
                                    <td>{{$profesor->name}}</td>
                                    <td>{{$profesor->rut}}</td>
                                </tr>
                            @endforeach

                        </Tbody>
                    </table>
                
                </div>

            </div>
            
        </div>
        <a href="{{route('trabajo.index')}}" style="float: right; padding: 20px;" class="btn btn-info">Volver</a>
    </div>
</div>

@endsection
