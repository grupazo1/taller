@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    VER USUARIO
                    <a href="{{route('usuario.edit', $usuario->id)}}" style="float:right" class="btn btn-primary">Editar</a>
                </div>
                <div class="card-body">
                <p><strong>Nombre del usuario: </strong>{{$usuario->name}}</p>
                <p><strong>Email del usuario: </strong>{{$usuario->email}}</p>
                <p><strong>Rol del usuario: </strong>{{$usuario->rol}}</p>
                <p><strong>Función del usuario: </strong>{{$usuario->rol_s}}</p>
                <p><strong>Carrera del usuario: </strong>{{$usuario->carrera}}</p>
                <p><strong>rut del usuario: </strong>{{$usuario->rut}}</p>

               
            </div>
            <a href="{{route('usuario.index')}}" class="btn btn-info">Volver</a>
        </div>
    </div>
</div>

@endsection