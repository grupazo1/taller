@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    EDITAR USUARIO
                </div>
                <div class="card-body">
                    {!! Form::model($usuario, ['route' => ['usuario.update', $usuario->id], 'method' => 'PUT']) !!}



                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$usuario->name}}" placeholder="{{$usuario->name}}" autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{$usuario->email}}" placeholder="{{$usuario->email}}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
                            <label for="rol" class="col-md-4 control-label">Rol</label>

                            <div class="col-md-6">
                                
                                 <select class= "form-control" name="rol" id="rol">
                                 <option value="{{$usuario->rol}}">Sin modificar</option>
                                 <option value="Estudiante">Estudiante</option>
                                 <option value="Profesor">Profesor</option>
                                 <option value="Secretaria">Secretaria</option>
                                 <option value="Encargado">Encargado</option>
                                 <option value="Encargad/profesor">Encargado/profesor</option>


                                 </select>
                           
                        </div>


                        


                        <div class="form-group{{ $errors->has('rol_s') ? ' has-error' : '' }}">
                            <label for="rol_s" class="col-md-4 control-label">Especialidad</label>

                            <div class="col-md-6">
                                
                                 <select class= "form-control" name="rol_s" id="rol_s">
                                 <option value="{{$usuario->rol_s}}">Sin modificar</option>
                                 <option value="Estudiante">Estudiante</option>
                                 <option value="Funcionario">Funcionario</option>
                                 </select>
                           
                        </div>
                        @if($usuario->rol ==  "Estudiante" or $usuario->rol ==  "Profesor")
                        <div class="form-group{{ $errors->has('carrera') ? ' has-error' : '' }}">
                            <label for="carrera" class="col-md-4 control-label">Carrera</label>

                            <div class="col-md-6">
                                
                                 <select class= "form-control" name="carrera" id="carrera">
                                 <option value="{{$usuario->carrera}}">Sin modificar</option>
                                 <option value="Ingeniería en computación e informática">Ingeniería en computación e informática</option>
                                 <option value="Ingeniería civil en computación e informática">Ingeniería civil en computación e informática</option>
                                 <option value="Ingeniería de Ejecución en Computación e Informática">Ingeniería de Ejecución en Computación e Informática</option>
                                 <option value="Ingeniería Industrial">Ingeniería Industrial</option>

                                 </select>
                           
                        </div>
                        @endif
                        <div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
                            <label for="rut" class="col-md-4 control-label">Rut</label>

                            <div class="col-md-6">
                                <input id="rut" type="rut" class="form-control" name="rut" value="{{$usuario->rut}}" placeholder="{{$usuario->rut}}">

                                @if ($errors->has('rut'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rut') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" value = "{{$usuario->password}}">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                            </div>
                        </div>




              
                  
                  
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection