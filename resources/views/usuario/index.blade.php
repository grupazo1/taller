@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class='col-md-8 col-md-offset-2'>
            <div class="card">
                <div class="card-header">
                    Lista de usuarios
                    <a href="{{route('register')}}" style="float:right"class="btn bn-sm btn-primary">Crear</a>
                </div>
            </div>

            <div class="card-body">           
                <table class='table table-striped table-hover'>
                    <thread>
                        <tr>
                            <th>ID</th>
                            <th>Nombre usuario</th>
                            <th>Usuario</th>
                            <th>Rol</th>
                            <th>Acciones</th>
                        </tr>
                    </thread>

                    <Tbody>
                        @foreach($usuarios as $usuario)
                            <tr>
                                @if($usuario->disponible == 1)
                                        <td>{{$usuario->id}}</td>   
                                         <td>{{$usuario->name}}</td> 
                                         <td>{{$usuario->email}}</td> 
                                         <td>{{$usuario->rol}}</td>     
                                         <td>
                                         <a   href="{{route('usuario.show', $usuario->id)}}" class="btn btn-sm btn-primary">Ver</a>
                                         <a  href="{{route('usuario.edit', $usuario->id)}}" class="btn btn-sm btn-primary">Editar</a >

                                        {!! Form::model($usuario, ['route' => ['estado', $usuario->id], 'method' => 'PUT']) !!}
                                            <button class="btn btn-sm btn-danger" onClick="confirmar_accion()">Eliminar</button>
                                        {!! Form::close() !!}

                                    </td>
                                @endif 
                            </tr>    
                                         
                        @endforeach
                                                                     
                    </Tbody>

                </table>
            </div>

        </div>
    </div>


</div>

<script>

function confirmar_accion(){

    return confirm('¿Estás seguro que deseas eliminar este usuario?');

}

</script>

@endsection