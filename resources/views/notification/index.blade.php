@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class='col-md-8 col-md-offset-2'>
                <div class="card">
                    <div class="card-header">
                                Lista de notificaciones
                    </div>
                </div>

                <div class="card-body">           
                    <table class='table table-striped table-hover'>
                        <thread>
                            <tr>
                                <th></th>
                                <th>Notificación</th>
                                <th>Nombre trabajo</th>
                                <th>Fecha</th>
                            </tr>
                        </thread>

                        <Tbody>


                            
                            @if (Auth::user()->rol=="Encargado/profesor" || Auth::user()->rol=="Profesor")
                                @foreach($trabajos as $trabajo)
                                    @if($trabajo->idPrimerProfesor == $usuario->id or $trabajo->idSegundoProfesor == $usuario->id)
                                        @foreach($notificaciones as $notificacion)
                                            <tr>
                                                @if($notificacion->trabajo_id == $trabajo->id)
                                                    @if($notificacion->autor_rol == 'Estudiante' or $notificacion->autor_rol == 'Secretaria')
                                                        <td>-</td>
                                                        <td>{{$notificacion->texto}}</td>
                                                        <td>{{$trabajo->nombre}}</td>
                                                        <td>{{$notificacion->created_at}}</td>
                                                    @endif
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif

                            @if (Auth::user()->rol=="Estudiante")
                                @foreach($trabajos as $trabajo)
                                    @if($trabajo->idPrimerEstudiante == $usuario->id or $trabajo->idSegundoEstudiante == $usuario->id or $trabajo->idTercerEstudiante == $usuario->id or $trabajo->idCuartoEstudiante == $usuario->id)
                                        @foreach($notificaciones as $notificacion)
                                            <tr>
                                                @if($notificacion->trabajo_id == $trabajo->id)
                                                    @if($notificacion->autor_rol == 'Profesor' or $notificacion->autor_rol == 'Secretaria')
                                                        <td>-</td>
                                                        <td>{{$notificacion->texto}}</td>
                                                        <td>{{$trabajo->nombre}}</td>
                                                        <td>{{$notificacion->created_at}}</td>
                                                    @endif
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                           
                        </Tbody>

                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection