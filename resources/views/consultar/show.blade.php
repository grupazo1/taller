@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class='col-md-8 col-md-offset-2'>
                <div class="card">
                    <div class="card-header">
                                VER HISTORIA DE REGISTROS
                    </div>
                </div>

                <div class="card-body">
                    <table class='table table-striped table-hover'>
                        <thread>
                            <tr>
                                <th>Fecha</th>
                                <th>Autor</th>
                                <th>Evidencia </th>
                            </tr>
                        </thread>

                        <tbody>
                    
                                <td>{{$trabajo->created_at}}</td>
                                <td>{{$usuario->name}}</td>
                                <td>{{$trabajo->id}}</td>

                        </tbody>

                    </table>
                </div>

                <div class="card-body">
                    <table class='table table-striped table-hover'>
                        <thread>
                            <tr>
                                <th>-</th>
                                <th>Avance</th>
                                <th>Evidencia </th>
                            </tr>
                        </thread>

                        <tbody>
                                @foreach($avances as $avance)
                                    <tr>
                                    @if($avance->trabajo_id == $trabajo->id)
                                        <td>-</td>
                                        <td>{{$avance->texto}}</td>
                                        @if($avance->file)
                                            <td>{{$avance->nombre_file}}</td>
                                        @else
                                            <td>No hay evidencia registrada.</td>
                                        @endif
                                    @endif
                                    </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection