@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class='col-md-8 col-md-offset-2'>
                <div class="card">
                    <div class="card-header">
                                BITACORAS
                    </div>
                </div>

                <div class="card-body">
                    <table class='table table-striped table-hover'>
                        <thread>
                            <tr>
                                <th>Nombre del Trabajo</th>
                                <th>Estudiante(s)</th>
                                <th>Profesor(es) guía</th>
                                <th>Historial de Registros </th>
                            </tr>
                        </thread>

                        <Tbody>
                        
                        @if (Auth::user()->rol=="Encargado/profesor" || Auth::user()->rol=="Profesor")
                            @foreach($trabajos as $trabajo)
                                    <tr>
                                        <td>{{$trabajo->nombre}}</td> 

                                        @foreach ($estudiantes as $estudiante)

                                            @if($estudiante->id == $trabajo->idPrimerEstudiante)
                                                <td>{{$estudiante->name}}</td>
                                            @endif
                                            @if($estudiante->id == $trabajo->idSegundoEstudiante)
                                                <td>{{$estudiante->name}}</td>
                                            @endif
                                            @if($estudiante->id == $trabajo->idTercerEstudiante)
                                                <td>{{$estudiante->name}}</td>
                                            @endif
                                            @if($estudiante->id == $trabajo->idCuartoEstudiante)
                                                <td>{{$estudiante->name}}</td>
                                            @endif
                                        @endforeach     

                                        @foreach ($profesores as $profesor)

                                            @if($profesor->id == $trabajo->idPrimerProfesor)
                                                <td>{{$profesor->name}}</td>
                                            @endif
                                            @if($profesor->id == $trabajo->idSegundoProfesor)
                                                <td>{{$profesor->name}}</td>
                                            @endif
                                        @endforeach    
                                        <td><a  href="{{route('trabajo.show', $trabajo->id)}}" class="btn btn-sm btn-primary">Ver avances</a></td>
                                    </tr>     
                                @endforeach
                        @endif

                        @if (Auth::user()->rol=="Estudiante")
                            @foreach($trabajos as $trabajo)
                                    <tr>
                                        <td>{{$trabajo->nombre}}</td>  
                                        <td>{{Auth::user()->name}}</td>
                                            
                                        @foreach ($profesores as $profesor)

                                            @if($profesor->id == $trabajo->idPrimerProfesor)
                                                <td>{{$profesor->name}}</td>
                                            @endif
                                            @if($profesor->id == $trabajo->idSegundoProfesor)
                                                <td>{{$profesor->name}}</td>
                                            @endif
                                        @endforeach    

                                        <td><a  href="{{route('consultarEvidencia', $trabajo->id)}}" class="btn btn-sm btn-primary">Ver avances</a></td>
                                        
                                    </tr>     
                                @endforeach
                        @endif


                        </Tbody>

                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection