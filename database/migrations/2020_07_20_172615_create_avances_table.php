<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avances', function (Blueprint $table) {
            $table->increments('id'); 
    
            $table->string('texto',255); //donde se escribirá el texto, ni idea cuanto tamaño tiene que ser
            $table->string('file',128)->nullable();
            $table->string('nombre_file',128)->nullable();

            $table->integer('trabajo_id')->unsigned(); //referencia al id de trabajo, ya que un trabajo puede tener muchos avances
            $table->integer('user_id')->unsigned(); //referencia al usuario, porque el usuario puede tener muchos avances

            
            //relaciones
            $table->foreign('trabajo_id')->references('id')->on('trabajos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avances');
    }
}
