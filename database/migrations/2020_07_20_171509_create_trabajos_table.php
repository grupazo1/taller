<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre', 255);

            $table->string('idPrimerEstudiante',255)->nullable();

            $table->string('idSegundoEstudiante',255)->nullable();

            $table->string('idTercerEstudiante',255)->nullable();

            $table->string('idCuartoEstudiante',255)->nullable();

            $table->string('idPrimerProfesor',255)->nullable();

            $table->string('idSegundoProfesor',255)->nullable();

            $table->string('razonFinalizar',255)->nullable();

            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            //relacion
            $table->enum('status',[0,1])->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabajos');
    }
}