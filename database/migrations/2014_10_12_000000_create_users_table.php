<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('email',128)->unique();
            $table->enum('rol',['Administrador','Estudiante','Profesor','Secretaria','Encargado','Encargado/profesor'])->default('Administrador');
            $table->enum('rol_s',['Estudiante','Funcionario'])->default('Estudiante');
            $table->enum('carrera',['No tiene carrera','Ingeniería en computación e informática','Ingeniería civil en computación e informática', 'Ingeniería de Ejecución en Computación e Informática','Ingeniería Industrial'])->default('Ingeniería en computación e informática');
            $table->string('rut',127)->unique();
            $table->enum('disponible',[0,1])->default(1);
            $table->string('password');
            $table->enum('trabajando',[0,1])->default(0);

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
