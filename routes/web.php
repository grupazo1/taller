<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('trabajo','TrabajoController');
Route::resource('avance','AvanceController');
Route::resource('comentario','ComentarioController');
Route::get('avance/crear/{id}', 'AvanceController@crear')->name('creando');
Route::get('avance/mostrar/{id}', 'AvanceController@mostrar')->name('avance.mostrar');
Route::get('consultar/{id}', 'TrabajoController@VerEvidencia')->name('consultarEvidencia');
Route::resource('usuario','UserController');
Route::resource('notification','NotificationController');
Route::put('/estado/{id}', 'UserController@estado')->name('estado');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('avance/registrarAvance/{id}','AvanceController@registrarAvance');
Route::put('trabajo/terminar/{id}','TrabajoController@terminar')->name('terminar');
Route::get('/finalizar/{id}','TrabajoController@finalizar')->name('finalizar');
Route::get('trabajo/ConsultarBitacoras/{id}','TrabajoController@ConsultarBitacoras')->name('consultar');
Route::get('trabajo/VerEvidencia/{id}','TrabajoController@VerEvidencia')->name('ver');
Route::post('/avance/storeComment','AvanceController@storeComment')->name('storeComment');
Route::get('avance/agregarFile/{id}', 'AvanceController@agregarFile')->name('agregarFile');
Route::get('sendEmailReminder','TrabajoController@sendEmailReminder');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
